#prerequisitos 

Consideraciones pruebas, este proyecto fue desarrollado gracias a la herramienta Laragon la cula provee un entorno de desarrollo. Para ejecutar el proyecto se debe tener previamente instalado:

    - Composer
    - php v7.4

#Instalación

    1.- Una vez clonado el repositorio, mediante consola se debe ubicar en la carpeta del proyecto. Una vez dentro de la carpeta del proyecto se debe ejecutar el siguiente comando:

        composer install
    
    2.- Una vez completada la instalación se debe copiar el archivo .env.example en con el nombre .env

    3.- Dentro del archivo .env se debe setear la variable de entorno API_TOKEN con el bearer token provisto por la api pública: https://gorest.co.in/

    4.- Luego, se deben generar las key del proyecto con el siguiente comando:

        php artisan key:generate

    5.- Finalmente, se puede ejecutar el proyecto con el siguiente comando:

        php artisan serve

#Consideraciones

Las metodos solicitados para la API REST fueron probados con Postman, basta con descargar la herramienta, crear una nueva colección y probar las rutas seleccionando el metodo asociado a la ruta (GET,POST,PUT o PATCH) pasar la header key: Accept y su valor asociado application/json
