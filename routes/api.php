<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/usuarios',[ApiController::class,'index']);
Route::post('/usuarios',[ApiController::class,'store']);
Route::put('/usuarios/{id}',[ApiController::class,'update']);
Route::patch('/usuarios',[ApiController::class,'updateEmail']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
