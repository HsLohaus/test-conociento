<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiUsuariosRequest;
use App\Http\Requests\StoreUsuarioApiRequest;
use App\Http\Requests\UpdateUsuarioRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    public function index(ApiUsuariosRequest $request)
    {
        try {
            $response = Http::withToken(env('API_TOKEN'))
                ->get('https://gorest.co.in/public/v2/users')
                ->throw();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()]);
        }

        $data = $response->collect();
        $filtered = null;

        $usuarios = $data->map(function ($usuario) {
            return [
                'nombre' => $usuario['name'],
                'email' => $usuario['email'],
                'genero' => $usuario['gender'],
                'activo' => $usuario['status'] == 'active' ? 'true' : ($usuario['status'] == 'inactive' ? 'false' : null),
            ];
        });

        if ($request->nombre) {
            $filtered = $usuarios->firstWhere('nombre', $request->nombre);
            if (!$filtered) {
                return response()->json(['message' => 'No se encuentran registros con ese nombre.']);
            }
        } elseif ($request->email) {
            $filtered = $usuarios->firstWhere('email', $request->email);
            if (!$filtered) {
                return response()->json(['message' => 'No se encuentran registros para ese email.']);
            }
        } elseif ($request->activo) {
            if ($request->activo == 'true') {
                $filtered = $usuarios->where('activo', 'true');
            } else {
                $filtered = $usuarios->where('activo', 'false');
            }
            $filtered->all();
            if (!$filtered) {
                return response()->json(['message' => 'No se encuentran registros para dicho estado.']);
            }
        }

        if ($filtered) {
            return response()->json($filtered);
        }

        return response()->json($usuarios, 200);
    }

    public function store(StoreUsuarioApiRequest $request)
    {
        try {
            $response = Http::withToken(env('API_TOKEN'))
                ->post('https://gorest.co.in/public/v2/users', [
                    'name' => $request->nombre,
                    'email' => $request->email,
                    'gender' => $request->genero,
                    'status' => $request->activo == 'true' ? 'active' : ($request->activo == 'false' ? 'inactive' : null),
                ])
                ->throw()
                ->json();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()]);
        }

        $usuario = [
            'nombre' => $response['name'],
            'email' => $response['email'],
            'genero' => $response['gender'],
            'activo' => $response['status'] == 'active' ? 'true' : ($response['status'] == 'inactive' ? 'false' : null),
        ];
        return response()->json($usuario);
    }

    public function update(UpdateUsuarioRequest $request, $id)
    {
        try {
            $response = Http::withToken(env('API_TOKEN'))
                ->put('https://gorest.co.in/public/v2/users/' . $id, [
                    'nombre' => $request->nombre,
                    'email' => $request->email,
                    'genero' => $request->genero,
                    'activo' => $request->activo,
                ])
                ->throw()
                ->json();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()]);
        }

        $usuario = [
            'nombre' => $response['name'],
            'email' => $response['email'],
            'genero' => $response['gender'],
            'activo' => $response['status'] == 'active' ? 'true' : ($response['status'] == 'inactive' ? 'false' : null),
        ];
        return response()->json($usuario);
    }

    public function updateEmail(Request $request)
    {
        try {
            $response = Http::withToken(env('API_TOKEN'))
                ->get('https://gorest.co.in/public/v2/users')
                ->throw();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()]);
        }

        $usuario = $response->collect()->firstWhere('email', $request->email);

        if (!$usuario) {
            return response()->json(['message' => 'No existe registro para ese email.']);
        }

        try {
            $response = Http::withToken(env('API_TOKEN'))
                ->put('https://gorest.co.in/public/v2/users/' . $usuario['id'], [
                    'nombre' => $request->nombre,
                    'email' => $request->email,
                    'genero' => $request->genero,
                    'activo' => $request->activo,
                ])
                ->throw()
                ->json();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()]);
        }

        $usuario = [
            'nombre' => $response['name'],
            'email' => $response['email'],
            'genero' => $response['gender'],
            'activo' => $response['status'] == 'active' ? 'true' : ($response['status'] == 'inactive' ? 'false' : null),
        ];
        return response()->json($usuario);
    }
}
